import axios from "axios"
import { getUser, setUser } from "../../AsyncStoreServices";
import { ENDPOINT_API } from "../../httpClient"
import { showMessage } from "../../showMessage";


const url =  'https://puskeslinggarjati.com/api'

export const uploadKtpAction = 
            (token, dataPhoto, navigation) => dispatch  =>
            {
                
                const file = new FormData();

            
                file.append('file', dataPhoto)
                axios.post(`${url}/pasien/updateFotoKtp`, file, {
                    headers: {
                        Authorization: token,
                        'Content-Type': 'multipart/form-data',
                    }
                }).then(resUpload => {

                    setUser({
                        status_ktp: 'Menunggu Konfirmasi'
                    })

                    showMessage('Berhasil upload foto KTP', 'success')


                    navigation.reset({index:0, routes:[{name: 'MainApp'}]})



                }).catch(errUpload => {

                    showMessage('Gagal upload foto KTP')
                    console.log('error', errUpload.message)
                })
            }

            