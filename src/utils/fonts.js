export const fonts = {
    regular : "sans-serif-regular",
    medium : "sans-serif-medium",
    bold : "sans-serif-condensed",
    semiBold: "Roboto-Bold"
}