import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { ImgCovid } from '../../assets/image'
import Gap from '../atoms/Gap'

const CardInformasiKesehatan = ({title,desc,img}) => {
  return (
    <View style={styles.container}>
        <View style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 10
        }}>
            <Image source={{uri: `https://puskeslinggarjati.com/public/uploads/${img}`}} style={styles.img} />
        </View>
      
        <Text style={{
            fontWeight: '600'
        }}>{title}</Text>
        <Gap height={15} />
        <Text style={{
            fontWeight: '300',
            fontSize: 14
        }}>{desc}</Text>
        <Gap height={15} />
    </View>
  )
}

export default CardInformasiKesehatan

const styles = StyleSheet.create({
    container:{
        borderBottomWidth: 0.2,
        marginBottom: 15
    },
    img:{
        width: 300,
        height: 150,
        borderRadius: 30,
        resizeMode: 'cover',
    
    }
})