import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const CardRiwayatObat = ({tanggal,riwayat_obat,rujukan,keluhan_pasien}) => {
  return (
    <View style={styles.container}>
      <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 20
      }}>
        <Text style={{
            marginRight: 20
        }}>Riwayat Obat</Text>
        <Text style={{
            maxWidth: 200
        }}>{riwayat_obat}</Text>
      </View>
      
    </View>
  )
}

export default CardRiwayatObat

const styles = StyleSheet.create({
    container:{
        marginBottom: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        backgroundColor: 'white',
        borderRadius: 20,
        paddingVertical: 10,
        paddingHorizontal: 15
    }
})