import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Header from '../components/atoms/Header'
import CardInformasiKesehatan from '../components/molecules/CardInformasiKesehatan'
import { ImgCovid, ImgPolaMakan } from '../assets/image'
import { useDispatch, useSelector } from 'react-redux'
import { getInformasiKesehatan } from '../utils/redux/action/pasien'
import { useEffect } from 'react'

const InformasiKesehatan = ({navigation}) => {

  const dispatch = useDispatch();
  const{informasiKesehatan} = useSelector(state => state.pasienReducer)

  const getDataInformasi = () => {
      dispatch(getInformasiKesehatan())
  }


  useEffect(() => {
    getDataInformasi();

    console.log('informasi kesehatan',informasiKesehatan)
  }, [informasiKesehatan])


  return (
    <View style={styles.container}>
        <Header
          title="Informasi Kesehatan"
          onBack={() => navigation.reset({index:0, routes:[{name:'MainApp'}] })}
        />
        <ScrollView>
            <View style={styles.content}>
                {informasiKesehatan.map(item => {
                    return(
                      <>
                        <CardInformasiKesehatan
                            key={item.id}
                            img={item.path_gambar}
                            title={item.judul_konten}
                            desc={item.deskripsi_konten}
                        />
                      </>
                    )
                })}

                {/* <CardInformasiKesehatan
                    img={ImgCovid}
                    title={"Informasi Covid 19"}
                    desc="Coronavirus merupakan keluarga besar virus yang menyebabkan penyakit pada manusia dan hewan. Pada manusia biasanya menyebabkan penyakit infeksi saluran pernapasan, mulai flu biasa hingga penyakit yang serius seperti Middle East Respiratory Syndrome (MERS) dan Sindrom Pernafasan Akut Berat/ Severe Acute Respiratory Syndrome (SARS). Coronavirus jenis baru yang ditemukan pada manusia sejak kejadian luar biasa muncul di Wuhan Cina, pada Desember 2019, kemudian diberi nama Severe Acute Respiratory Syndrome Coronavirus 2 (SARS-COV2), dan menyebabkan penyakit Coronavirus Disease-2019 (COVID-19).
                    "
                />
                <CardInformasiKesehatan
                    img={ImgPolaMakan}
                    title={"Tips Pola Makan Sehat"}
                    desc="Mungkin prinsip “4 Sehat 5 Sempurna” telah menjadi slogan yang terus terngiang dalam benak Anda. Namun, seiring berjalannya waktu dan berkembangnya ilmu pengetahuan, slogan ini tidak lagi sesuai dengan kehidupan zaman sekarang.

                    Kementerian Kesehatan RI kini telah mengeluarkan penggantinya, yaitu Pedoman Gizi Seimbang (PGS). PGS menerapkan prinsip konsumsi makanan yang beraneka ragam diiringi aktivitas fisik, perilaku hidup bersih, dan pemantauan berat badan secara teratur.
                    
                    Pedoman ini mengatur pola makan sehat dengan Tumpeng Gizi Seimbang untuk memperbaiki prinsip “4 sehat 5 sempurna”. Bagian dasar tumpeng yang paling lebar berisikan bahan makanan pokok yang perlu dikonsumsi dalam jumlah paling besar."
                /> */}
            </View>
       </ScrollView>
    </View>
  )
}

export default InformasiKesehatan

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
      },
      content:{
        paddingHorizontal: 20,
        marginTop: 20
      }
})