import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Header from '../components/atoms/Header';
import { useState } from 'react';
import { useEffect } from 'react';
import { ENDPOINT_API } from '../utils/httpClient';
import axios from 'axios';
import { getUser } from '../utils/AsyncStoreServices';
import CardRiwayatObat from '../components/molecules/CardRiwayatObat';

const RiwayatObatScreen = ({navigation}) => {
  const[data,setData] = useState([])

  const fetchData = async () =>{
      const user = await getUser();
      const id = user.id;

      var result = await axios.get(`${ENDPOINT_API}/pasien/riwayat-obat?id_pasien=${id}`)
      .then(res => {
        setData(res.data.data);
      }).catch(err => {
        console.log(err.message)
      })

    return Promise.resolve(result)
  }

  useEffect(() => {
      fetchData();
  },[])
  return (
    <View style={styles.container}>
        <Header
          title="Riwayat Obat"
          onBack={() => navigation.reset({index:0, routes:[{name:'MainApp'}] })}
        />
        <ScrollView>
        <View style={styles.content}>
        {data.map(item => {

            return(
              <CardRiwayatObat
                key={item.id}

                riwayat_obat={item.resep_obat}
              />
            )
          })}
        </View>
        </ScrollView>

    </View>
  )
}

export default RiwayatObatScreen

const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: 'white'
  },
  content:{
    paddingHorizontal: 20,
    marginTop: 20
  }
})