import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Header from '../components/atoms/Header'
import { normalizeFont } from '../utils/normalizeFont'
import Gap from '../components/atoms/Gap'
import CardRiwayatKesehatan from '../components/molecules/CardRiwayatKesehatan'
import { useState } from 'react'
import { getUser } from '../utils/AsyncStoreServices'
import axios from 'axios'
import { ENDPOINT_API } from '../utils/httpClient'
import { useEffect } from 'react'

const RiwayatKesehatanScreen = ({navigation}) => {
  const[data,setData] = useState([])

  const fetchData = async () =>{
      const user = await getUser();
      const id = user.id;

      var result = await axios.get(`${ENDPOINT_API}/pasien/riwayatkesehatan?id_pasien=${id}`)
      .then(res => {
        setData(res.data.data);
      }).catch(err => {
        console.log(err.message)
      })

    return Promise.resolve(result)
  }

  useEffect(() => {
      fetchData();
  },[])

  return (
    <View style={styles.container}>
        <Header
          title="Riwayat Kesehatan"
          onBack={() => navigation.reset({index:0, routes:[{name:'MainApp'}] })}
        />
        <ScrollView>
        <View style={styles.content}>
          {data.map(item => {
            var tanggal = item.created_at;
            var parse = new Date(tanggal);
            var tanggal = parse.getDate()
            var bulan = parse.getMonth();
            var tahun = parse.getFullYear();

            var fullTanggal =tanggal+'-'+bulan+'-'+tahun

            return(
              <CardRiwayatKesehatan
                key={item.id}
                keluhan_pasien={item.keluhan_pasien}
                rujukan={item.rujukan}
                tanggal={fullTanggal}
                riwayat_obat={item.resep_obat}
              />
            )
          })}
          

        </View>
        </ScrollView>

    </View>
  )
}

export default RiwayatKesehatanScreen

const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: 'white'
  },
  content:{
    paddingHorizontal: 20,
    marginTop: 20
  }
})