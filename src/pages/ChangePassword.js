import { Alert, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Header from '../components/atoms/Header'
import UseForm from '../utils/useForm'
import CustomButton from '../components/molecules/CustomButton'
import { color } from '../utils/colors'
import CustomTextInput from '../components/molecules/CustomTextInput'
import Gap from '../components/atoms/Gap'
import { useEffect } from 'react'
import { useState } from 'react'
import { getUser } from '../utils/AsyncStoreServices'
import { showMessage } from '../utils/showMessage'
import axios from 'axios'
import { ENDPOINT_API } from '../utils/httpClient'
import firebaseSetup from '../utils/firebaseSetup'
import firebase from 'firebase'

const ChangePassword = ({navigation}) => {
    const[idPasien, setIdPasien] = useState('');
    const[email,setEmail] = useState('')

    const[form, setForm] = UseForm({
        id_pasien: idPasien,
        password: '',
        current_password: '',
    })




    const user = async () => {
        const user = await getUser();
        const id = user.id;
        setEmail(user.email);
        setForm('id_pasien', id)

        return Promise.resolve(user)
    }

    const reauth  = async (currentPassword) => {
        var user = await firebase.auth().currentUser;
        console.log(user)

        var cred = await firebase.auth.EmailAuthProvider.credential(email, currentPassword);
        console.log('cred',cred)
    
        
        return user.reauthenticateWithCredential(cred);
    }

    const onSubmit = () => {
        if(form.password == "")
        {
            showMessage('Password tidak boleh kosong')
            return false
        }else{

            // reauth(form.current_password)

            reauth(form.current_password).then(res => {
                var user = firebase.auth().currentUser;
                user.updatePassword(form.password).then(res => {
                    axios.post(`${ENDPOINT_API}/pasien/changePassword`, form)
                    .then(res => {
                        
                        showMessage('Berhasil mengganti password', 'success')
        
                        navigation.reset({index:0, routes:[{name:'MainApp'}] })
                    }).catch(err => {
                        showMessage(err.message);
                    })
                }).catch(err => {
                    showMessage('Password lama salah', 'danger')
                })

            }).catch(err => {
                Alert.alert(err.message)
            })


        }
    }

    const fetchUserFirebase = () => {
        var user = firebase.auth().currentUser;
        
        console.log(user)
        return user;
    }

    


    useEffect(() => {
        user();
        fetchUserFirebase()
    }, [idPasien])
  

  return (
    <View style={styles.container}>
        <Header
            title={"Ubah Password"}
            onBack={() => navigation.goBack()}
        />

        <View style={styles.content}>
        <CustomTextInput
            text="Password Lama"
            placeholder="Silahkan masukan password lama anda..."
            value={form.current_password}
            onChangeText={(value) => setForm('current_password', value)}
            secureTextEntry
        
        />

        <Gap height={20} />

        <CustomTextInput
            text="Password Baru"
            placeholder="Silahkan masukan password baru anda..."
            value={form.password}
            onChangeText={(value) => setForm('password', value)}
            secureTextEntry
        
        />

        <Gap height={30} />
        <CustomButton 
            color={color.primary}
            text="Ubah Password"
            onPress={onSubmit}
        />
        </View>
    </View>
  )
}

export default ChangePassword

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
    },
    content:{
        flex: 1,
        paddingHorizontal: 20,
        marginTop: 20
    }
})