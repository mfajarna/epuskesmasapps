import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Header from '../components/atoms/Header'
import { useState } from 'react'
import { getUser } from '../utils/AsyncStoreServices'
import axios from 'axios'
import { ENDPOINT_API } from '../utils/httpClient'
import { useEffect } from 'react'
import CardSuratRujukan from '../components/molecules/CardSuratRujukan'

const SuratRujukanScreen = ({navigation}) => {
    const[data,setData] = useState([])

    const fetchData = async () =>{
        const user = await getUser();
        const id = user.id;
  
        var result = await axios.get(`${ENDPOINT_API}/suratrujukan/getSuratRujukan?id_pasien=${id}`)
        .then(res => {
            console.log('data', res.data.data)
          setData(res.data.data);
        }).catch(err => {
          console.log(err.message)
        })
  
      return Promise.resolve(result)
    }

    useEffect(() => {
        fetchData();
    },[])
  return (
    <View style={styles.container}>
        <Header
          title="Surat Rujukan"
          onBack={() => navigation.reset({index:0, routes:[{name:'MainApp'}] })}
        />
        <ScrollView>
        <View style={styles.content}>
            {data.map(item => {
                var tanggal = item.created_at;
                var parse = new Date(tanggal);
                var tanggal = parse.getDate()
                var bulan = parse.getMonth();
                var tahun = parse.getFullYear();
            
                var fullTanggal =tanggal+'-'+bulan+'-'+tahun
                var nameFile = item.name_file;

                return(
                    <CardSuratRujukan
                        key={item.id}
                        no_surat={item.no_surat}
                        tanggal={fullTanggal}
                        onPress={() => navigation.navigate('LihatSuratRujukan', nameFile)}
                    />

                )
            })}
        </View>
        </ScrollView>

    </View>
  )
}

export default SuratRujukanScreen

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
      },
      content:{
        paddingHorizontal: 20,
        marginTop: 20
      }
})