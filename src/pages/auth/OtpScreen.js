import { Alert, KeyboardAvoidingView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import { normalizeFont } from '../../utils/normalizeFont';
import { color } from '../../utils/colors';
import { useDispatch } from 'react-redux';
import { registerAction, setLoading } from '../../utils/redux/action';
import auth from '@react-native-firebase/auth'
import Header from '../../components/atoms/Header';
import { IlOtpVerif } from '../../assets/illustration';
import Gap from '../../components/atoms/Gap';
import { showMessage } from '../../utils/showMessage';
import axios from 'axios';
import { ENDPOINT_API } from '../../utils/httpClient';
import { setUser, storeData } from '../../utils/AsyncStoreServices';
import firebaseSetup from '../../utils/firebaseSetup';


const OtpScreen = ({route,navigation}) => {

  const {phone,dataRegister,profile_pasien, token} = route.params;
 
  const dispatch = useDispatch()
  const lengthInput = 6;
  let textInput = useRef(null)
  let clockCall = null
  let defaultCountdown = 10

  const[intervalVal, setIntervalVal] = useState("")
  const[countdown, setCountDown] = useState(defaultCountdown)
  const[enableResend, setEnableResend] = useState(false)
  const[confirm,setConfirm] = useState(null);
  const[focusInput, setFocusInput] = useState(true)
  


  useEffect(() => {
      textInput.focus()

      console.log(dataRegister)
  }, [])



  useEffect(() => {
    dispatch(setLoading(false))

    clockCall = setInterval(() => {
      decrementClock()

    }, 1000)

    return () => {
      clearInterval(clockCall)
    }
  })

  const decrementClock = () => {

    if(countdown === 0)
    {
      setEnableResend(true)
      setCountDown(0)
      clearInterval(clockCall)
    }else{
      setCountDown(countdown - 1)
    }

    
  }

  const onChangeFocus = () => {
    setFocusInput(true)
  }

  const onChangeBlur = () => {
    setFocusInput(true)
  }


  const onChangeText = (val) => {
    setIntervalVal(val)
  }

  const onChangeNumber = () => {
    setIntervalVal("")

    navigation.goBack();
    

  }

  const onResendOTP = async () => {
      if(enableResend)
      {
         try{


           setCountDown(defaultCountdown)
           setEnableResend(false)
           clearInterval(clockCall)
  
          clockCall = setInterval(() => {
            decrementClock()
        }, 1000)

         }catch(error)
        {
          showMessage(error.message)
        }

      }


      
  }

  const onVerification = async () => {
      try{
        
        dispatch(setLoading(true))

        const data = {
            no_handphone: phone,
            otp_number: intervalVal,
        }



        await axios.post(`${ENDPOINT_API}/pasien/verificationOtp`, data)
                .then(res => {

                  const response = res.data.data;

                  if(response == "failed otp not same")
                  {
                    showMessage('Kode OTP invalid!');
                  }else{
                    const id = profile_pasien.id
                    setUser({
                      id: id,
                      token: token,
                      nama_lengkap: profile_pasien.nama_lengkap,
                      alamat: profile_pasien.alamat,
                      jenis_kelamin: profile_pasien.jenis_kelamin,
                      no_ktp: profile_pasien.no_ktp,
                      no_handphone: profile_pasien.no_handphone,
                      email: profile_pasien.email,
                      device_token: profile_pasien.device_token,
                      is_verification: profile_pasien.is_verification,
                      authenticated: true
                  })

                  axios.get(`${ENDPOINT_API}/pasien/fetchStatusKtp?id=${id}`, {
                    headers: {
                        Authorization: token
                    }
                }).then(res => {

                  console.log(res)
                  firebaseSetup.auth().createUserWithEmailAndPassword(profile_pasien.email, dataRegister.password)
                  .then(res => {
          
                    console.log('res fbase', res)
                    const data = {
                      displayName: dataRegister.nama_lengkap,
                      email: dataRegister.email,
                      uid: res.user.uid,
                      password: dataRegister.password
                    }
                  
                    // console.log("status fbase", res);
                    firebaseSetup.database()
                          .ref('users/' + res.user.uid + '/')
                          .set(data);
                      
                    storeData('user', data)
                    
                  }).catch(err => {
                    console.log(err)
                  })
    
                    dispatch({
                        type: "SET_STATUS_VERIFIKASI_KTP",
                        value: res.data.data.status
                    })
                }).catch(errKtp => {
                    console.log(errKtp.response)
                })

                dispatch(setLoading(false))
                showMessage("Selamat anda berhasil membuat akun baru!", "success")
                
                navigation.reset({index: 0, routes: [{name: 'MainApp'}]});
                 }
                }).catch(err => {
                  showMessage(err.message)
              })

      }catch(error)
      {
        dispatch(setLoading(false))
        // showMessage(error.message)

        Alert.alert(error.message);
        
      }
  }



  return (
    <View style={styles.container}>

      <Header
        title="Konfirmasi OTP"
        onBack={() => navigation.goBack()}
      />
      <ScrollView>
      <KeyboardAvoidingView
          behavior='padding'
          keyboardVerticalOffset={50}
          style={styles.containerAvoiddingView}
        >
          <Gap height={20} />
          <IlOtpVerif />

          <Gap height={20} />
          <Text style={styles.titleStyle}>
            Masukan kode OTP yang dikirimkan via SMS ke nomor {phone}
          </Text>

          <View>
            <TextInput
              ref = {(input) => textInput = input}
              onChangeText={onChangeText}
              style={
                {width: 200, height: 100, backgroundColor: 'white',
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                
                elevation: 5,}
              }
              value={intervalVal}
              maxLength={lengthInput}
              returnKeyType="done"
              keyboardType='numeric'
              onFocus={onChangeFocus}
              onBlur={onChangeBlur}
              autoFocus={focusInput} 
            />
          </View>
{/* 
          <View style={styles.containerInput}>
            {
              Array(lengthInput).fill().map((data, index) => (
                  <View 
                      key={index}
                      style={[
                          styles.cellView,
                          {
                            backgroundColor: index === intervalVal.length ? color.primary : '#F6F5FA'
                          }
                        ]}>

                    <Text 
                      style={styles.cellText}
                      onPress={() => textInput.focus()}
                    >
                      {intervalVal && intervalVal.length > 0 ? intervalVal[index] : ""}
                    </Text>

                  </View>
              ))
            }

          </View> */}




        </KeyboardAvoidingView>


            

        <View style={styles.viewVerifikasi}>
              <View style={styles.bottomView}> 
                  <TouchableOpacity onPress={onChangeNumber}>
                    <View style={styles.btnChangeNumber}>
                        <Text style={styles.textChange}>Ganti nomor</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={onResendOTP}>
                    <View style={styles.btnResend}>
                        <Text style={[
                          styles.textResend,
                          {
                            color: enableResend ? '#3987E5' : 'grey'
                          }
                          
                          ]}>Resend OTP ({countdown})</Text>
                    </View>
                  </TouchableOpacity>
              </View>
                <TouchableOpacity onPress={onVerification}>
                            <View style={
                                styles.btnVerifikasi
                              }>
                              <Text style={styles.textContinue}>Verifikasi</Text>
                            </View>
                </TouchableOpacity>
        </View>
      </ScrollView>

      
    </View>
  )
}

export default OtpScreen

const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FFFF',
  },
  containerAvoiddingView:{
    alignItems: 'center',
    padding: 10,
    flex: 1,
  },
  titleStyle:{
    margin: 0,
    marginBottom: 20,
    fontSize: 14,
    textAlign: 'center',
  
  },
  containerInput: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cellView:{
    paddingVertical: 13,
    width: 45,
    height: 25,
    margin: 7,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    
    backgroundColor: '#F6F5FA',
    paddingVertical: 30,
    shadowColor: "#000",
      shadowOffset: {
          width: 0,
          height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 8,
  },
  cellText:{
    textAlign: 'center',
    fontSize: 16,
  },
  bottomView:{
    flexDirection: 'row',
    
  },
  btnChangeNumber:{
    width: 150,
    height: 50,
    borderRadius: 10,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  textChange: {
    color: '#3987E5',
    alignItems: 'center',
    fontSize: 15
  },
  btnResend: {
    width: 150,
    height: 50,
    borderRadius: 10,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  textResend: {
    alignItems: 'center',
    fontSize: 15
  },
  btnVerifikasi:{
    width: 250,
    height: 50,
    borderRadius: 10,
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: '#3987E5'
  },
  viewVerifikasi: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 25,
    alignItems: 'center'
  },
  textContinue: {
    color: 'white', 
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 15
  }
})