import { SafeAreaView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Header from '../components/atoms/Header'
import Gap from '../components/atoms/Gap'
import CardDokterChat from '../components/molecules/CardDokterChat'
import { useState } from 'react'
import firebaseSetup from '../utils/firebaseSetup'
import { useEffect } from 'react'
import { getData } from '../utils/AsyncStoreServices'

const MessageScreen = ({navigation}) => {
  const [dokter, setDokter] = useState([]);

  const getUserData = () => {
    getData('user').then(res => {
      const data = res;
    });
  };


  const getDokter = () => {
    firebaseSetup.database()
      .ref('dokter/')
      // .limitToLast(3)
      .once('value')
      .then(res => {
        if (res.val()) {
          const oldData = res.val();
          const data = [];
          Object.keys(oldData).map(key => {
            data.push({
              id: key,
              data: oldData[key],
            });
          });
         
          setDokter(data);
        }
      })
      .catch(err => {
        showError(err.message);
      });
  }

  const [user, setUser] = useState({});
  const [historyChat, setHistoryChat] = useState([]);

  useEffect(() => {
    getDokter()
    getUserData();
    navigation.addListener('focus', () => {
    getUserData();
    });

    getDataUserFromLocal();
    const rootDB = firebaseSetup.database().ref();
    const urlHistory = `messages/${user.uid}/`;
    const messagesDB = rootDB.child(urlHistory);

    console.log('ini dokter', dokter.data)

    messagesDB.on('value', async snapshot => {
      if (snapshot.val()) {
      const oldData = snapshot.val();
      const data = [];

      const promises = await Object.keys(oldData).map(async key => {
        const urlUidAdmin = `dokter/${oldData[key].uidPartner}`;
        const detailAdmin = await rootDB.child(urlUidAdmin).once('value');
        data.push({
          id: key,
          detailAdmin: detailAdmin.val(),
          ...oldData[key],
        });
        console.log(data[1].detailAdmin.email)
      });

      await Promise.all(promises);

      setHistoryChat(data);
    }
  });
  },[])


  const getDataUserFromLocal = () => {
    getData('user').then(res => {
      setUser(res);
    });
  };

  return (
    <View style={styles.container}>
        <Header
            title={"Chat Dokter"}
        />
        <View style={styles.content}>
            <Text style={{
              fontSize: 15,
              fontWeight: '600'
            }}>Kirim pesan konsultasi dengan dokter!</Text>
            <Gap height={20} />
            {dokter.map(item => {
              return(
                <CardDokterChat
                  key={item.id}
                  namaDokter={"Yessica Juliane"}
                  onPress={() => navigation.navigate('ChattingScreen', item)}
                />
              )
            })}
            
        </View>
    </View>
  )
}

export default MessageScreen

const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: 'white'
  },
  content:{
    marginTop: 20,
    flex: 1,
    paddingHorizontal: 20,
  }
})