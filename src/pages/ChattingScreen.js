import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React, { useState } from 'react'
import { getData } from '../utils/AsyncStoreServices';
import { useRef } from 'react';
import firebaseSetup from '../utils/firebaseSetup';
import Header from '../components/atoms/Header';
import InputChat from '../components/molecules/InputChat';
import ChatItem from '../components/molecules/ChatItem';
import { useEffect } from 'react';
import { getChatTime, setDateChat } from '../utils/date';

const ChattingScreen = ({navigation,route}) => {
    const dataDokter = route.params;
    // const device_token = dataDokter.data.device_token;
    const [chatContent, setChatContent] = useState('');
    const [user, setUser] = useState({});
    const [chatData, setChatData] = useState([]);
    const scrollView= useRef();
    const [namaUser,setNamaUser] = useState('');

    const getDataUserFromLocal = () => {
        getData('user').then(res => {
        setUser(res);
        setNamaUser(res.email)
        });
     };

     const chatSend = () =>{
        const today = new Date();

        const data = {
           sendBy: user.uid,
           chatDate: today.getTime(),
           chatTime: getChatTime(today),
           chatContent: chatContent,
        };

        const chatID = `${user.uid}_${dataDokter.data.uid}`;
        const urlFirebase = `chatting/${chatID}/allChat/${setDateChat(today)}`;
        const urlMessageUser = `messages/${user.uid}/${chatID}`;
        const urlMessageDoctor = `messages/${dataDokter.data.uid}/${chatID}`;

       const dataHistoryChatForUser = {
       lastContentChat: chatContent,
       lastChatDate: today.getTime(),
       uidPartner: dataDokter.data.uid,
       };

      const dataHistoryChatForDoctor = {
       lastContentChat: chatContent,
       lastChatDate: today.getTime(),
       uidPartner: user.uid,
       };

       firebaseSetup.database()
     .ref(urlFirebase)
     .push(data)
     .then(() => {
       setChatContent('');
       // set history for user
       firebaseSetup.database()
         .ref(urlMessageUser)
         .set(dataHistoryChatForUser);

       // set history for dataDoctor
       firebaseSetup.database()
         .ref(urlMessageDoctor)
         .set(dataHistoryChatForDoctor);
     })
     .catch(err => {
       showError(err.message);
     });

    //  const dataJson = JSON.stringify({
    //    "to": `${device_token}`,
    //    "priority": "high",
    //    "soundName": "default",
    //    "notification": {
    //        "title": "Admin Hasnida",
    //        "body": `Ada pesan baru dari ${namaUser}`
       
    //        }
    //  });

    //  axios.post('https://fcm.googleapis.com/fcm/send', dataJson,{
    //    headers:{
    //      Authorization: 'key=AAAAMn40zS0:APA91bG50ySLS-gl3e0KKQbKB51F01Q2x70opMDt6X1Se0l6zZ0oaaUvTDZ2Nj8240pFfKHBmB9Pe-BKSHM05hkC1hvbH6hkmEYLZ6m0MDoXZY7hM1WM0T0hn1elhdz0s-NzXPv-76PN',
    //      "content-type": "application/json",
    //    }
    //  }).then(res => {
    //    console.log(res);
    //  }).catch(err =>{
    //    console.log(err.message)
    //  })
   }

   useEffect(() =>{
    getDataUserFromLocal();
    const chatID = `${user.uid}_${dataDokter.data.uid}`;
    const urlFirebase = `chatting/${chatID}/allChat/`;

    firebaseSetup.database()
  .ref(urlFirebase)
  .on('value', snapshot => {
    if (snapshot.val()) {
      const dataSnapshot = snapshot.val();
      const allDataChat = [];
      Object.keys(dataSnapshot).map(key => {
        const dataChat = dataSnapshot[key];
        const newDataChat = [];

        Object.keys(dataChat).map(itemChat => {
          newDataChat.push({
            id: itemChat,
            data: dataChat[itemChat],
          });
        });

        allDataChat.push({
          id: key,
          data: newDataChat,
        });
      });
      setChatData(allDataChat);
    }
  })
}, [dataDokter.data.uid, user.uid])



  return (
        <View style={styles.pages}>
            <View>
            <Header 
            title="Chat Dokter"
            onBack={() => navigation.goBack()} />
            </View>
            
            <View style={styles.wrapperMessage}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          ref={scrollView}
          onContentSizeChange={() => scrollView.current.scrollToEnd({ animated: false })}
          >
          {chatData.map(chat => {
            return (
              <View key={chat.id}>
                <Text style={styles.chatDate}>{chat.id}</Text>
                {chat.data.map((itemChat) => {
                  const isMe = itemChat.data.sendBy === user.uid;
                  return (
                    <ChatItem key={itemChat.id}
                        isMe={isMe}
                        text={itemChat.data.chatContent}
                        date={itemChat.data.chatTime}
                    />
                  );
                })}
              </View>
            );
          })}
        </ScrollView>
            </View>

            <InputChat
              value={chatContent}
              onChangeText={value => setChatContent(value)}
              onButtonPress={chatSend}
              targetChat={dataDokter}
            
            />
        </View>
  )
}

export default ChattingScreen

const styles = StyleSheet.create({
    pages: {
        flex: 1,
    },
    wrapperMessage: {
        flex:1,
        backgroundColor: '#FAFAFA'
    },
    category:{
        paddingHorizontal: 25,
        paddingVertical: 10,
    },
    chatDate: {
    fontSize: 11,
    color: '#8D92A3',
    marginVertical: 20,
    textAlign: 'center',
  },
})