import { Alert, ScrollView, StyleSheet, Text, TouchableOpacity, View, DevSettings } from 'react-native'
import React, { useEffect, useState } from 'react'
import { deleteUser, getData, getUser, setUser } from '../utils/AsyncStoreServices'
import { showToast } from '../utils/showToast'
import Toast from 'react-native-toast-message'
import { color } from '../utils/colors'
import { fonts } from '../utils/fonts'
import { IcNotif, IcUser } from '../assets/icon'
import Gap from '../components/atoms/Gap'
import CustomButton from '../components/molecules/CustomButton'
import FiturComponent from '../components/molecules/FiturComponent'
import { useDispatch, useSelector } from 'react-redux'
import { setLoading } from '../utils/redux/action'
import axios from 'axios'
import { showMessage } from '../utils/showMessage'
import { ENDPOINT_API } from '../utils/httpClient'

const HomeScreen = ({navigation}) => {

  const[name,setName] = useState('');
  const[verifktp,setVerifKtp] = useState(false);
  const dispatch = useDispatch();
  const[isStatusKtp, setIsStatusKtp] = useState('');
  const[statusKtp, setStatusKtp] = useState('');
  const[noAntrian, setNoAntrian] = useState();
  const[createdAt, setCreatedAt] = useState('');
  const[statusAntrian, setStatusAntrian] = useState();
  const[data,setData] = useState()
  const[waktu,setWaktu] = useState('')
  const[status,setStatus] = useState(false);
  const[nextDay, setNextDay] = useState("");
  const jamBuka = 8;
  const jamTutup = 11;
  const jamsekarang = 9;

  const settingApps = () => {
    var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    var date = new Date();
    var dayName = days[date.getDay() + 1];

    console.log(dayName)

    const d = new Date();
    let hour = d.getHours();

    console.log('hari', dayName)

      if(hour >= jamBuka && hour < jamTutup)
      {
        setStatus(true)
        console.log('buka')
      }
      else{
        setStatus(false)
        setNextDay(dayName)
      }
  }

  // get data user
  const user = async () => {
        dispatch(setLoading(true))
        const dataUser = await getUser();
    
        setVerifKtp(dataUser.is_verificationktp)
        setName(dataUser.nama_lengkap)

       dispatch(setLoading(false))

       return Promise.resolve(dataUser)
  }

  const setWaktuKunjungan = () =>{
    var d1 = new Date (createdAt),
    d2 = new Date ( d1 );
    
    let timeAdd = d2.setMinutes ( d1.getMinutes() + 30 );
    const d = new Date(timeAdd);
    

    let time = ('0' + d.getHours()).slice(-2) + '.'
              + ('0' + d.getMinutes()).slice(-2)

    setWaktu(time)

    return time;
  }



  const fetchStatusKtp = async () => {
    dispatch(setLoading(false))
    const user = await getUser();

    const token = user.token;
    const getStatusKtp = user.status_ktp;
    var statusKtp;


    axios.get(`${ENDPOINT_API}/pasien/fetchKtp`, {
      headers: {
          Authorization: token,
      }
  }).then(res => {
      setStatusKtp(res.data.data.status)
      setIsStatusKtp(res.data.data.status)
      statusKtp = res.data.data.status

      if(statusKtp == null)
      {
        DevSettings.reload();
      }else{
        setUser({
          status_ktp: statusKtp
        })
      }


  }).catch(err => {
      console.log('ktp',err.message)
  })

  if(getStatusKtp == "Belum Upload KTP")
  {
     showToast('Verififkasi KTP anda untuk menggunakan fitur, klik notif!','error', onDanger)
  }

  if(getStatusKtp == "Menunggu Konfirmasi")
  {
     showToast('KTP Anda sedang menunggu konfirmasi','error', onDanger)
  }

}

  const onDanger = () => {
      Toast.hide();
      navigation.reset({index:0, routes:[{name:'MyProfileScreen', params: {isStatusKtp}}] })
  }

  const onDaftarAntrian = () => {
      Alert.alert("Hallo")
  }

  const fetchAntrianPasien = async () => {
      const dataUser = await getUser();
      const id = dataUser.id;

      var result = await axios.get(`${ENDPOINT_API}/antrianpasien/get-antrian?id_pasien=${id}`)
            .then(res => {
              var data = res.data.data;
              var createdat = res.data.data.created_at;
              setCreatedAt(createdat)

              var date = new Date(createdat)
              var hours = date.getHours();


              console.log('jam',hours)

              setNoAntrian(data.no_urut)
              setStatusAntrian(data.status_pemeriksaan)
            }).catch(err => {
              console.log(err.message)
            })

      return Promise.resolve(result)
  }

  const renderStatus = (statusktp) => {
    if(noAntrian == null)
    {
      return(
        <>
            <Text style={styles.statusAntrian}>
                Status Antrian Anda
              </Text>
              <Gap height={14} />
              <Text style={styles.descAntrian}>
                    Anda untuk saat ini <Text style={{color: '#E97D19'}}>belum memesan </Text> 
                    antrian online, tekan tombol dibawah ini 
                    untuk mendapatkan antrian online.
              </Text>
              <Gap height={14} />
              <View style={{paddingHorizontal: 62}}>
            </View>
        </>
      )
    }
    else{
      return(
        <>
              <Text style={styles.statusAntrian}>
                  Detail Antrian Pemeriksaan Pasien
              </Text>
              <Gap height={14} />
              <Text style={styles.descAntrian}>
                    No Antrian: <Text style={{color: '#E97D19', fontSize: 15}}>{noAntrian} </Text> 
              </Text>
              <Text style={styles.descAntrian}>
              Dengan Status: <Text style={{color: '#E97D19', fonts: 15}}>{statusAntrian} </Text>
              </Text>
              <Gap height={20} />
              <Text style={styles.descAntrian}>
                
                  {renderTulisanAntrian()}
              </Text>
        </>
      )
    }
  }

  const renderTulisanAntrian = () => {
      if(status == true)
      {
        return(
          <>
            <Text>  Harap segera datang pada {waktu} ke poliklinik untuk memulai pemeriksaan</Text>
          </>
        )
      }else{
        return(
          <>
            <Text>  Harap segera datang pada hari {nextDay} jam 07.30 ke poliklinik untuk memulai pemeriksaan</Text>
          </>
        )

      }
  }


  useEffect(() => {
    settingApps()
    dispatch(setLoading(false))
    user();
    setWaktuKunjungan()
    fetchStatusKtp()
    fetchAntrianPasien()

    if(isStatusKtp == null)
    {
      dispatch(setLoading(true))
    }
    else{

      dispatch(setLoading(false))
    }
    
  }, [isStatusKtp,verifktp,name])
  



  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>


      <View style={styles.topContent}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View style={{justifyContent: 'center',}}>
            <IcUser />
          </View>
          
          <View style={{marginRight: 75}}>
            <Text style={styles.name}>Hai, {name}</Text>
            <Text style={styles.desc}>Selamat Datang Kembali</Text>
          </View>
          <View style={{justifyContent: 'center',}}>
        
          </View>
        </View>

        <View style={styles.antrianWrapper}>
            {renderStatus()}
        </View>

      </View>
      <View style={{
        marginTop: 30,
        paddingHorizontal:50
      }}>
        {noAntrian == null ? <CustomButton
                          text="Daftar Antrian"
                          color={color.primary}
                          onPress={() => navigation.navigate("PemeriksaanScreen")}
                          disabled={isStatusKtp == "Sudah Konfirmasi" ? false : true}
                      /> : <></>}
      </View>

      
      <Gap height={30} />

      <View style={styles.contentFitur}>
        <Text style={{
          
          fontSize: 16,
          color: 'black'
        }}>Pelayanan kami</Text>


      <Gap height={19} />
        <FiturComponent 
              onPress={() => navigation.navigate("PemeriksaanScreen")}
              title="Pemeriksaan"
              desc="Pilih pemeriksaan sesuai dengan gejala yang anda
              alami"
              disabled={isStatusKtp == "Sudah Konfirmasi" && noAntrian == null ? false : true}
            />

      <Gap height={13} />
        <FiturComponent 
              onPress={() => navigation.navigate("RiwayatKesehatanScreen")}
              title="Riwayat Kesehatan"
              desc="Lihat riwayat kesehatan anda sesuai dengan pemeriksaan terakhir"
              disabled={isStatusKtp == "Sudah Konfirmasi" ? false : true}
          />

      <Gap height={13} />
        <FiturComponent 
        
              onPress={() => navigation.navigate("InformasiKesehatanScreen")}
              title="Informasi Kesehatan"
              desc="Informasi tentang dunia kesehatan"
              disabled={isStatusKtp == "Sudah Konfirmasi" ? false : true}
          />

      <Gap height={13} />
        <FiturComponent 
              onPress={() => navigation.navigate("RiwayatObatScreen")}
              title="Riwayat Obat"
              desc="Riwayat penggunaan obat sesuai dengan
              pemeriksaan"
              disabled={isStatusKtp == "Sudah Konfirmasi" ? false : true}
          />

      <Gap height={13} />
        <FiturComponent 
              onPress={() => navigation.navigate("SuratRujukanScreen")}
              title="Surat Rujukan"
              desc="Lihat surat rujukan untuk puskesmas"
              disabled={isStatusKtp == "Sudah Konfirmasi" ? false : true}
          />

      <Gap height={13} />
      </View>
      </ScrollView>
    </View>
  )
}

export default HomeScreen

const styles = StyleSheet.create({
    container:{
      backgroundColor: '#F1F5FD',
      flex: 1
    },
    topContent: {
      height: 215,
      backgroundColor: color.primary,
      paddingHorizontal: 26,
      paddingTop: 23
    },
    name:{

      color: color.white,
      fontSize: 20
    },
    desc:{
      color: color.white,
      fontSize: 13
    },
    antrianWrapper:{
        paddingVertical: 15,
        marginTop: 40,
        height: 200,
        backgroundColor: 'white',
        borderRadius: 10,

        shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 5,
      },
      shadowOpacity: 0.34,
      shadowRadius: 6.27,

      elevation: 10,
    },
    statusAntrian:{
      
      color: 'black',
      fontSize: 16,
      textAlign: 'center'
    },
    descAntrian:{

      color: '#9F9F9F',
      fontSize: 13,
      textAlign: 'center',
      paddingHorizontal: 16
    },
    contentFitur:{
      marginTop: 70,
      paddingHorizontal: 26
    }

})