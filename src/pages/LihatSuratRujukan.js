import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Header from '../components/atoms/Header'
import { WebView } from 'react-native-webview';

const LihatSuratRujukan = ({navigation, route}) => {
  const params = route.params;

  console.log('params', params)

  return (
    <View style={styles.container}>
        <Header
          title="Surat Rujukan"
          onBack={() => navigation.goBack()}
        />
            <WebView
                source={{ uri: 'https://puskeslinggarjati.com/public/uploads/suratrujukan/'+params }}
            />
  
    </View>
  )
}

export default LihatSuratRujukan

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'white'
      },
      content:{
        paddingHorizontal: 20,
        marginTop: 20
      }
})